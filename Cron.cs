// Cron
// A basic cron implementation in C#
//
// Minutes ............. Y ............. 0-59 .......................... , - *
// Hours ............... Y ............. 0-23 .......................... , - *
// Day of month ........ Y ............. 1-31 .......................... , - *
// Month ............... Y ............. 0-11 or JAN-DEC ............... , - *
// Day of week ......... Y ............. 1-7 or SUN-SAT ................ , - *

namespace Utilities
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.IO;
	using System.Linq;

	public class Cron
	{
		static void Main(string[] args)
		{
			var crontabfiles = new List<string>(args);
			var baseCrontab = string.Format("{0}/crontab", Directory.GetCurrentDirectory());
			var crontab = new List<string>();

			if (!crontabfiles.Contains(baseCrontab))
			{
				crontabfiles.Add(baseCrontab);
			}

			if (crontabfiles.Count == 1 && !File.Exists(baseCrontab))
			{
				throw new Exception("Could not find base crontab file.");
			}

			foreach (string crontabfile in crontabfiles)
			{
				foreach (string expression in File.ReadLines(crontabfile))
				{
					if (!expression.Trim().StartsWith("#") && expression.Trim().Length > 0)
					{
						crontab.Add(expression.Trim());
					}
				}
			}

			if (!crontab.Any())
			{
				throw new Exception("No cron expressions found, bailing.");
			}

			var cron = new Cron(crontab);
			cron.Run();
		}

		public List<string> Expressions { get; set; }

		public Cron(List<string> Expressions) 
		{
			this.Expressions = Expressions;
		}

		public void Run()
		{
			while (true)
			{
				foreach (var expression in this.Expressions)
				{
					var parsedExpression = this.ParseExpression(expression);

					if (this.Validate(parsedExpression))
					{
						Console.WriteLine("Running: {0} @ {1}", parsedExpression[Fields.Command], DateTime.Now.ToString());

						// Execute the statement and log any errors.
						try
						{
							Process.Start(parsedExpression[Fields.Command]);
						}
						catch(Exception e)
						{
							Console.WriteLine(e);
						}
					}
				}

				// Sleep for 1 minute
				System.Threading.Thread.Sleep(60000);
			}
		}

		public Dictionary<Fields, string> ParseExpression(string expression)
		{
			expression = expression.Trim();

			// Pull apart our cron expression and repackage it into something easier to use.
			var fields = new Dictionary<Fields, string>();

			// Ignore crontab comment lines
			if (expression.Trim().StartsWith("#"))
			{
				return fields;
			}

			// Split on blank spaces, supporting only the standard cron fields makes parsing this a lot simpler.
			var splitExpression = expression.Split(' ');

			// There must be at least 6 arguments, 5 for the date parts and one for the command.

			if (splitExpression.Length < 6)
			{
				Console.WriteLine("Invalid number of arguments");
			}

			fields.Add(Fields.Minutes, splitExpression[0].Trim());
			fields.Add(Fields.Hours, splitExpression[1].Trim());
			fields.Add(Fields.DayOfMonth, splitExpression[2].Trim());
			fields.Add(Fields.Month, splitExpression[3].Trim());
			fields.Add(Fields.DayOfWeek, splitExpression[4].Trim());
			fields.Add(Fields.Command, expression.Replace(splitExpression[0] + " " + splitExpression[1] + " " + splitExpression[2] + " " + splitExpression[3] + " " + splitExpression[4], string.Empty).Trim()); // ugly, but easy. like your mom.

			return fields;
		}

		public bool Validate(Dictionary<Fields, string> parsedExpression)
		{
			bool minutesValid = this.Validate(parsedExpression[Fields.Minutes], Fields.Minutes);
			bool hoursValid = this.Validate(parsedExpression[Fields.Hours], Fields.Hours);
			bool dayOfMonthValid = this.Validate(parsedExpression[Fields.DayOfMonth], Fields.DayOfMonth);
			bool monthValid = this.Validate(parsedExpression[Fields.Month], Fields.Month);
			bool dayOfWeekValid = this.Validate(parsedExpression[Fields.DayOfWeek], Fields.DayOfWeek);

			return minutesValid && hoursValid && dayOfMonthValid && monthValid && dayOfWeekValid;
		}

		/// <summary>
		/// Validate a sub-expression as a cron field.
		/// This method works by normalizing the values to compare to into a List<string> and then comparing 
		/// the equivalent part of DateTime.now against all values in the list.
		/// This method handles normalizing named months and days of the week prior to validating, so its 
		/// equally valid to pass MON,TUE 1,2 for the DayOfWeek value for example.
		/// </summary>
		/// <param name="subExpression">An individual value, a list, a * wildcard, or a range describing valid values.</param>
		/// <param name="field">Which cron expression field to transform this one as, and validate it against.</param>
		/// <returns>Return true if the sub expression matches now.</returns>
		public bool Validate(string subExpression, Fields field)
		{
			// Wildcard support
			if (subExpression == "*")
			{
				return true;
			}

			// Store a normalized list to compare DateTime.Now against.
			var comparisonList = new List<string>();

			// Convert named fields to their numeric equivalents.
			if (field == Fields.Month)
			{
				subExpression.Replace("JAN", "0");
				subExpression.Replace("FEB", "1");
				subExpression.Replace("MAR", "2");
				subExpression.Replace("APR", "3");
				subExpression.Replace("MAY", "4");
				subExpression.Replace("JUN", "5");
				subExpression.Replace("JUL", "6");
				subExpression.Replace("AUG", "7");
				subExpression.Replace("SEP", "8");
				subExpression.Replace("OCT", "9");
				subExpression.Replace("NOV", "10");
				subExpression.Replace("DEC", "11");
			}

			if (field == Fields.DayOfWeek)
			{
				// cheaper to just replace named days with numbers than to check for them.
				subExpression.Replace("SUN", "0");
				subExpression.Replace("MON", "1");
				subExpression.Replace("TUE", "2");
				subExpression.Replace("WED", "3");
				subExpression.Replace("THU", "4");
				subExpression.Replace("FRI", "5");
				subExpression.Replace("SAT", "6");
			}

			if (field == Fields.Minutes && subExpression.StartsWith("*/"))
			{
				// Verify 60 minutes can be evenly divided by
				// the number after the /. By doing this we always start on 0
				// and we always end at 60 minutes, perfect because we don't
				// track the last runtime.
				var divisor = subExpression.Split('/').Last();

				if (60 % int.Parse(divisor) != 0)
				{
					throw new Exception("Minute divisor must equally divide into an hour.");
				}

				for (int i = 0; i < 60; i++)
				{
					if (i % int.Parse(divisor) == 0)
					{
						comparisonList.Add(i.ToString());
					}
				}
			}
			else
			{
				// Split lists of values into comparisonList.
				if (subExpression.Contains(","))
				{
					comparisonList = subExpression.Split(',').ToList<string>();
				}

				// Convert ranges to lists first, and then split them into comparisonList.
				if (subExpression.Contains("-"))
				{
					var range = subExpression.Split('-');
					var min = range[0];
					var max = range[1];

					for (int i = int.Parse(min); i <= int.Parse(max); i++)
					{
						comparisonList.Add(i.ToString());
					}
				}

			}

			// If nothing in comparisonList assume its an individual value and add it to comparisonList.
			if (!comparisonList.Any())
			{
				comparisonList.Add(subExpression);
			}

			// By now, regardless of whether we started with a list or a range of values, named or numeric, 
			// we should now have a list of numbers (as strings) to compare the part to.
			var now = DateTime.Now;

			foreach (var comparisonItem in comparisonList)
			{
				switch (field)
				{
					case Fields.Minutes:
						if (now.Minute.ToString() == comparisonItem.Trim())
						{
							return true;
						}
						break;
					case Fields.Hours:
						if (now.Hour.ToString() == comparisonItem.Trim())
						{
							return true;
						}
						break;
					case Fields.DayOfMonth:
						if (now.Day.ToString() == comparisonItem.Trim())
						{
							return true;
						}
						break;
					case Fields.Month:
						if (now.Month.ToString() == comparisonItem.Trim())
						{
							return true;
						}
						break;
					case Fields.DayOfWeek:
						if (now.DayOfWeek.ToString() == comparisonItem.Trim())
						{
							return true;
						}
						break;
				}
			}

			return false;
		}

		public enum Fields
		{
			Minutes = 0,
			Hours = 1,
			DayOfMonth = 2,
			Month = 3,
			DayOfWeek = 4,
			Command = 5
		}
	}
}
