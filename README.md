## Cron

A simplistic implementation of cron in C#.

This is an earlier (Version 7 Unix) style cron, it runs every minute, checks to see if any jobs
are ready to run, runs them, then sleeps until time to run again.

### Features

* Default crontab loads from running directory.
* Pass in any number of crontab paths and expressions will load from all of them.
* Parsing and validation of cron expressions.
* Process.Start() to kick off a process, sample included calls shell script in linux and os x which touches a file every minute.
* Supports named and numeric months and days of week.
* Supports wildcards (*), ranges (0-5), and lists (1,2,3) for all supported fields.
* Supported fields: Minute, Hour, Day of Month, Month, Day of Week, and a Command to run.

### Todo

* Logging
* Reload dirty crontabs
* Possibly (eventually) add ? and / operators